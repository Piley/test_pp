<?php

namespace app\controllers;

use Yii;
use yii\data\ArrayDataProvider;

class DialogController extends \yii\web\Controller
{
    /* Show all user dialogs */
    public function actionIndex()
    {
        $ig = $this->instagramRequest();
        $response = json_decode($ig->direct->getInbox());

        $dialogs = [];
        $count = 0;

        foreach ($response->inbox->threads as $thread) {
            if ($count >= 10) break;
            $dialogs[] = [
                'thread_id' => $thread->thread_id,
                'thread_title' => $thread->thread_title,
                'last_activity_at' => $thread->last_activity_at/1000000,
                'last_permanent_item' => $thread->last_permanent_item->text ? $thread->last_permanent_item->text : 'Вложение',
            ];
            $count ++;
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $dialogs,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /* Show user dialog by id */
    public function actionView($id)
    {
        $ig = $this->instagramRequest();
        $response = json_decode($ig->direct->getThread($id));

        $messages = [];
        $count = 0;

        foreach ($response->thread->items as $message) {
            if ($count >= 10) break;
            $messages[] = [
                'item_id' => $message->item_id,
                'user_id' => $message->user_id,
                'text' => $message->text ? $message->text : $message->item_type,
                'item_type' => $message->item_type,
            ];
            $count ++;
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $messages,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('view', [
            'id' => $id,
            'dataProvider' => $dataProvider,
        ]);
    }

    private function instagramRequest() {
        \InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
        $ig = new \InstagramAPI\Instagram(false, true);
        $ig->login(Yii::$app->params['instagram']['login'], Yii::$app->params['instagram']['password']);
        return $ig;
    }

}
