<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
?>
<h1>Dialog № <?= $id?></h1>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'item_id',
        'user_id',
        'text',
    ],
]) ?>