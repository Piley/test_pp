<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Html;

?>
<h1>Dialogs</h1>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'label' => 'Thread ID',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::a($data['thread_id'], ['/dialog/view', 'id' => $data['thread_id']]);
            },
        ],

        'thread_title',
        'last_activity_at:datetime',
        'last_permanent_item',
    ],
]) ?>